package endpoints;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.esi.bovespadata.facade.BovespaFacade;
import br.com.esi.bovespadata.pojo.Acao;

@Path("/consulta")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AcaoResource {

	@POST
	@Path("/ativo")
	public AcaoResponse consultas(String[] param){
		
		AcaoResponse resp = new AcaoResponse();
		
		BovespaFacade f = new BovespaFacade();

		List<Acao> acoes = f.getAcoes(param);
		
		resp.setAcoes(acoes);
		
		return resp;
		
	}
	
}
