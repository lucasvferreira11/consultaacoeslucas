package endpoints;

import java.util.LinkedList;
import java.util.List;

import br.com.esi.bovespadata.pojo.Acao;

public class AcaoResponse {

	private List<Acao> acoes = new LinkedList<Acao>();

	public AcaoResponse(){
		
	}
	
	
	
	public AcaoResponse(List<Acao> acoes) {
		super();
		this.acoes = acoes;
	}



	public List<Acao> getAcoes() {
		return acoes;
	}

	public void setAcoes(List<Acao> acoes) {
		this.acoes = acoes;
	}
	
	
	
}
