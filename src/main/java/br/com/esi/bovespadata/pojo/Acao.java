package br.com.esi.bovespadata.pojo;

public class Acao {

    private String sigla, descricao;
    private double valorAtual, percAtual, valorBase;

    public Acao(){}

    public Acao(String sigla, String descricao, double valorAtual, double percAtual, double valorBase) {
        this.sigla = sigla;
        this.descricao = descricao;
        this.valorAtual = valorAtual;
        this.percAtual = percAtual;
        this.valorBase = valorBase;
    }
    
    public Acao(String sigla, String descricao, double valorAtual, double percAtual) {
        this.sigla = sigla;
        this.descricao = descricao;
        this.valorAtual = valorAtual;
        this.percAtual = percAtual;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValorAtual() {
        return valorAtual;
    }

    public void setValorAtual(double valorAtual) {
        this.valorAtual = valorAtual;
    }

    public double getPercAtual() {
        return percAtual;
    }

    public void setPercAtual(double percAtual) {
        this.percAtual = percAtual;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }
}
