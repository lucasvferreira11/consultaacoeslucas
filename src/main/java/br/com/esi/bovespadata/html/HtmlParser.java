package br.com.esi.bovespadata.html;

import java.util.List;

public interface HtmlParser<T> {

	public List<T> parse (String html);
}
