package br.com.esi.bovespadata.html;

import java.util.ArrayList;
import java.util.List;

import br.com.esi.bovespadata.pojo.Acao;
import br.com.esi.bovespadata.utils.HtmlUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

public class HtmlParserImp implements HtmlParser <Acao> {

	public List <Acao> parse(String html) {
		Document htmlDocument = Jsoup.parse(html);
		
		System.out.println(html);
		Elements lines = getShareNodes (htmlDocument);
		List <Acao> acoes = new ArrayList<Acao>();
		for (Element line : lines) {
			String name = getName (line);
			String initials= getInitials(line);
			Double value = getValue(line);
			Double variation = getVariation(line);
			Acao acao = new Acao(initials, name, value, variation);
			acoes.add(acao);
		}
		return acoes;
	}

	
	private Elements getShareNodes (Document htmlDocument) {
		return Xsoup.compile("//div[@class='grid-body']//div[@class='line ']").evaluate(htmlDocument).getElements();
	}

	
	private String getName (Element element) {
		return Xsoup.compile("//div[contains(@class,'nome_ativo')]//span//text()").evaluate(element).get();
	}
	
	private String getInitials (Element element) {
		List <String> values = Xsoup.compile("//div[contains(@class,'period')]//button//@data-cod_ativo").evaluate(element).list();
		if (values.size()>0) return values.get(0);
		else throw new RuntimeException("Initials not found");
	}
	
	private Double getValue (Element element) {
		String stringValue = Xsoup.compile("//div[contains(@class,'valor_ultimo')]//a//text()").evaluate(element).get();
		return new Double (stringValue.trim().replace(",", "."));
	}
	
	private Double getVariation (Element element) {
		String stringValue = Xsoup.compile("//div[contains(@class,'variacao')]//a//text()").evaluate(element).get();
		return new Double(stringValue.trim().replace(",", "."));
	}

	
}
