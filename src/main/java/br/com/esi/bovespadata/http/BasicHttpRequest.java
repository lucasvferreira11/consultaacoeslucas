package br.com.esi.bovespadata.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClients;

public class BasicHttpRequest {
	
	private HttpClient client;
	
	public BasicHttpRequest () {
		this.client = HttpClients.createDefault();
	}
	
	public HttpResponse get (URI uri, Header ... headers) {
		HttpGet getRequest = new HttpGet(uri);
		getRequest.setHeaders(headers);
		return execute (getRequest);
	}
	
	
	private HttpResponse execute (HttpUriRequest request) {
		try {
			return this.client.execute(request);
		}
		catch (ClientProtocolException e) {
			throw new RuntimeException("Failed to connect to URI: " + e.getMessage());
		}
		catch (IOException e) {
			throw new RuntimeException("Connection problem: " + e.getMessage());
		}		
	}
	
	protected InputStream extractContent (HttpResponse response ) {
		try {
			return response.getEntity().getContent();
		} catch (UnsupportedOperationException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
