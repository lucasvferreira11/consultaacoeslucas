package br.com.esi.bovespadata.http;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicHeader;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class BovespaRequest extends BasicHttpRequest{
	private final String GET_SHARES_PATH = "api-indicadores-economicos/group-content/3/9";
	
	public BovespaRequest () {
	}
	
	
	private String getHost () {
		return "economia.estadao.com.br";
	}
	
	private String getProtocol () {
		return "http";
	}
	
	public String getShares ()  {
		URI uri = createURI(getHost(), GET_SHARES_PATH, getProtocol());
		HttpResponse response = get(uri, 
				new BasicHeader(HttpHeaders.ACCEPT,"*/*"),
				new BasicHeader("Accept-Encoding", "gzip, deflate, br"),
				new BasicHeader("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4"),
				new BasicHeader("Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"));
		InputStream stream = extractContent(response);
		
		JsonObject json = getAsJson(stream);
		return json.get("html").getAsString();
	}
	
	private JsonObject getAsJson (InputStream inputStream) {
		InputStreamReader reader = new InputStreamReader(inputStream);
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(reader);
		return element.getAsJsonObject();	
	}
	
	private URI createURI (String host, String path, String scheme, NameValuePair...parameters ) {
		try {
			URI uri=new URIBuilder()
					.setHost(host)
					.setPath(path)
					.setScheme(scheme)
					.build();
			return uri;
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(),e.getCause());
		}		
	}
	
	
}
