package br.com.esi.bovespadata.facade;

import java.util.ArrayList;
import java.util.List;

import br.com.esi.bovespadata.html.HtmlParser;
import br.com.esi.bovespadata.html.HtmlParserImp;
import br.com.esi.bovespadata.http.BovespaRequest;
import br.com.esi.bovespadata.pojo.Acao;
import br.com.esi.bovespadata.utils.HtmlUtils;

public class BovespaFacade {
	
	/**
	 * Obtém todas as ações da bovespa
	 * @return Lista com todas as acoes da Bovespa
	 */
	public  List<Acao> getAcoes (){
		BovespaRequest request = new BovespaRequest();
		String html = HtmlUtils.removeScapeCharacters(request.getShares());
		HtmlParser<Acao> parser = new HtmlParserImp();
		return parser.parse(html);
	}

	/**
	 * Obtém apenas as ações que correspondam às siglas passadas por paramentro
	 * @param acoes Siglas das ações
	 * @return Ações correspondentes às siglas recebidas
	 */
	public List<Acao> getAcoes (String...acoes) {
		List <Acao> a = getAcoes ();
		List <Acao> filter = new ArrayList<Acao>();
		for (Acao foundAcao:a) {
			for (String wantedAcao: acoes) {
				if (foundAcao.getSigla().equalsIgnoreCase(wantedAcao)) filter.add(foundAcao);
			}
		}
		return filter;
	}
}
