package br.com.esi.bovespadata.facade;

import java.util.List;

import br.com.esi.bovespadata.pojo.Acao;

public class Main {


	public static void main(String[] args) {

		BovespaFacade f = new BovespaFacade();
		
		String[] a = new String[2];
		a[0] = "PETR4";
		a[1] = "LAME4";
		
		List<Acao> acoes = f.getAcoes(a);
		
		for(Acao ac : acoes){
			System.out.println(ac.getValorAtual());
		}

		
	}
		
	
}
